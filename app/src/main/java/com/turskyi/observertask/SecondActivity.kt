package com.turskyi.observertask

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.turskyi.observertask.interfaces.RepositoryObserver
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity(), RepositoryObserver {

    private lateinit var userDataRepository: UserDataRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        btn_data.setOnClickListener {
            userDataRepository.getNewDataFromRemote()

        }

        btn_first_activity.setOnClickListener { onBackPressed() }

        userDataRepository = UserDataRepository.getInstance()
        userDataRepository.registerObserver(this)
    }

    @SuppressLint("SetTextI18n")
    override fun onUserDataChanged(name: String, age: Int) {

        name_view.text = "Full name: $name"
        age_view.text = "Age: $age"
    }

    override fun onDestroy() {
        super.onDestroy()
        userDataRepository.removeObserver(this)
    }
}
