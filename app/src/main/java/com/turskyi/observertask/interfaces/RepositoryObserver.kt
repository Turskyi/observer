package com.turskyi.observertask.interfaces

interface RepositoryObserver {
    fun onUserDataChanged(name: String, age : Int)
}