package com.turskyi.observertask.interfaces

interface Subject {

    fun registerObserver(repositoryObserver: RepositoryObserver)
    fun removeObserver(repositoryObserver: RepositoryObserver)
    fun notifyObservers()
    fun getNewDataFromRemote()
}