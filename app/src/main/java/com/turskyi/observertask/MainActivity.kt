package com.turskyi.observertask

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.turskyi.observertask.interfaces.RepositoryObserver
import kotlinx.android.synthetic.main.activity_main.*

/** set the data in first and second activity at the same time */
class MainActivity : AppCompatActivity(R.layout.activity_main), RepositoryObserver {

    private lateinit var userDataRepository: UserDataRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userDataRepository = UserDataRepository.getInstance()
        userDataRepository.registerObserver(this)

        btn_data.setOnClickListener {
            userDataRepository.getNewDataFromRemote()
        }

        btn_second_activity.setOnClickListener {
            val intent = Intent(this@MainActivity, SecondActivity::class.java)
            startActivity(intent)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onUserDataChanged(name: String, age: Int) {
        name_view.text = "Full name: $name"
        age_view.text = "Age: $age"
    }

    override fun onDestroy() {
        super.onDestroy()
        userDataRepository.removeObserver(this)
    }
}