package com.turskyi.observertask

import android.os.Handler
import com.turskyi.observertask.interfaces.RepositoryObserver
import com.turskyi.observertask.interfaces.Subject

class UserDataRepository : Subject {

    companion object {
        private var INSTANCE: UserDataRepository? = null
        fun getInstance(): UserDataRepository {
            INSTANCE ?: run {
                INSTANCE = UserDataRepository()
            }
            return INSTANCE!!
        }
    }

    private var person: Person = Person("",0)

    private var arrayOfObservers: ArrayList<RepositoryObserver> = ArrayList()

    override fun getNewDataFromRemote() {
        val handler = Handler()
        handler.postDelayed({
            "Bob".setUserData(32)
        }, 100)
    }

    override fun registerObserver(repositoryObserver: RepositoryObserver) {
        if (!arrayOfObservers.contains(repositoryObserver)) {
            arrayOfObservers.add(repositoryObserver)
            notifyObservers()
        }
    }

    override fun removeObserver(repositoryObserver: RepositoryObserver) {
        if (arrayOfObservers.contains(repositoryObserver)) {
            arrayOfObservers.remove(repositoryObserver)
        }
    }

    override fun notifyObservers() {
        for (observer in arrayOfObservers) {
            observer.onUserDataChanged(person.name, person.age)
        }
    }

    private fun String.setUserData(age: Int) {
        this@UserDataRepository.person.name = this
        this@UserDataRepository.person.age = age
        notifyObservers()
    }
}