package com.turskyi.observertask

data class Person (
    var name: String,
    var age: Int
)